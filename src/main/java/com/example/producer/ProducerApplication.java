package com.example.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@SpringBootApplication
public class ProducerApplication {

    private final static String QUEUE_NAME = "MeasurementQueue";

    public static void main(String[] args) throws Exception {
        String[] dataRead = new String[1000];
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (BufferedReader br = new BufferedReader(new FileReader("C:/Users/corfu/Desktop/Queue/producer/sensor.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                int i = 0;
                String[] values = line.split(",");
                dataRead[i] = values[i];

                try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
                    channel.queueDeclare(QUEUE_NAME, false, false, false, null);

                    JSONObject object = new JSONObject();
                    object.put("timeStamp", Timestamp.valueOf(LocalDateTime.now())).toString();
                    object.put("energyConsumption", dataRead[i]);
                    object.put("sensor_id", args[0]);

                    String objToString = object.toString();
                    String exchangeName = "";
                    String routingKey = QUEUE_NAME;
                    channel.basicPublish(exchangeName, routingKey, null, objToString.getBytes());
                    System.out.println("Sent : " + object + " to the queue.");
                }
                Thread.sleep(10000);
                i++;
            }
        }
    }
}